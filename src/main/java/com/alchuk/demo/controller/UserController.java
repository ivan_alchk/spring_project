package com.alchuk.demo.controller;

import com.alchuk.demo.entity.User;

import com.alchuk.demo.exceptions.UserAlreadyExists;
import com.alchuk.demo.service.userservice.UserCreatorService;
import com.alchuk.demo.service.userservice.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping
public class UserController {


    @Autowired
    @Qualifier("userCreator")
    private UserCreatorService userCreatorService;

    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @ExceptionHandler({NullPointerException.class, UserAlreadyExists.class})
    public String handleException(Exception e) {
        return e.getMessage();
    }


    @PostMapping("/signUp")
    public User signUp(@RequestParam(value = "userName") String userName, @RequestParam(value = "password") String password, HttpSession session) {

        User newUser = userService.findUserByUserNameAndPassword(userName, password).orElse(null);

        if (newUser == null) {
            newUser = userCreatorService.create(userName, password);
            userService.saveUser(newUser);
            session.setAttribute("id", newUser.getId());
        } else {
            throw new UserAlreadyExists("such user already exists");
        }


        return newUser;
    }

    @PostMapping("/logIn")
    public User logIn(@RequestParam("userName") String userName, @RequestParam("password") String password, HttpSession session) {
        User toLogIn = userService.findUserByUserNameAndPassword(userName, password).orElse(null);
        if (toLogIn == null) {
            throw new NullPointerException("Such user doesn`t exist");
        }
        session.setAttribute("id", toLogIn.getId());
        return toLogIn;
    }

    @GetMapping("/getAllUsers")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }


}
