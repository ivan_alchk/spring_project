package com.alchuk.demo.controller;

import com.alchuk.demo.entity.Topic;
import com.alchuk.demo.service.topicservice.TopicCreatorService;
import com.alchuk.demo.service.topicservice.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
@RequestMapping
public class TopicController {


    @Autowired
    @Qualifier("topicCreator")
    private TopicCreatorService topicCreatorService;

    @Autowired
    @Qualifier("topicService")
    private TopicService topicService;


    @PostMapping("createTopic")
    public Topic createNewTopic(@RequestParam("topicName") String topicName){

        Topic newTopic = topicCreatorService.create(topicName);
        topicService.saveTopic(newTopic);

        return newTopic;
    }

}
