package com.alchuk.demo.controller;

import com.alchuk.demo.entity.Post;
import com.alchuk.demo.entity.User;
import com.alchuk.demo.service.postservice.PostCreatorService;
import com.alchuk.demo.service.postservice.PostService;
import com.alchuk.demo.service.userservice.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpSession;
import java.util.List;


@RequestMapping
@RestController
public class PostController {

    @Autowired
    @Qualifier("postCreator")
    private PostCreatorService postCreatorService;

    @Autowired
    @Qualifier("postService")
    private PostService postService;

    @Autowired
    @Qualifier("userService")
    private UserService userService;


    @PostMapping("/addPost")
    public Post addPost(@RequestParam("topic") String topicName, @RequestParam("text") String text, HttpSession session){
        User author = userService.getUserById((Long)session.getAttribute("id"));
        String authorUserName = author.getUserName();
        Post newPost = postCreatorService.create(authorUserName, topicName, text);
        postService.savePost(newPost);
        return newPost ;
    }

    @GetMapping("/findByTopic")
    public List<Post> findAllByTopic(@RequestParam("topic") String topic){
        return postService.findAllPostsByTopic(topic);
    }

    @GetMapping("/findByAuthor")
    public List<Post> findAllByAuthor(@RequestParam("author") String author){
        return postService.findAllPostsByAuthor(author);
    }



}
