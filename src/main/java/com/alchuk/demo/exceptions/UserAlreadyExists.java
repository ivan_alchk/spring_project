package com.alchuk.demo.exceptions;

public class UserAlreadyExists extends RuntimeException {
    private final String message;

    public UserAlreadyExists(String message){
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
