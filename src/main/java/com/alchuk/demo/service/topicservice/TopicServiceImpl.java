package com.alchuk.demo.service.topicservice;

import com.alchuk.demo.dao.topicdao.TopicDao;
import com.alchuk.demo.entity.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Access;

@Service("topicService")
public class TopicServiceImpl implements TopicService {

    @Autowired
    private TopicDao topicDao;

    @Override
    public void saveTopic(Topic topic) {
        topicDao.save(topic);

    }
}
