package com.alchuk.demo.service.topicservice;

import com.alchuk.demo.entity.Topic;

public interface TopicCreatorService {

    Topic create(String topicName);
}
