package com.alchuk.demo.service.topicservice;

import com.alchuk.demo.entity.Topic;
import org.springframework.stereotype.Service;

@Service("topicCreator")
public class TopicCreatorServiceImpl implements TopicCreatorService {

    @Override
    public Topic create(String topicName) {
        Topic topic = new Topic();
        topic.setTopicName(topicName);
        return topic;
    }
}
