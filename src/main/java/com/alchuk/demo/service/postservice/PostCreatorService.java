package com.alchuk.demo.service.postservice;

import com.alchuk.demo.entity.Post;

public interface PostCreatorService {
    Post create(String authorUserName, String topicName, String text);
}
