package com.alchuk.demo.service.postservice;

import com.alchuk.demo.entity.Post;
import com.alchuk.demo.entity.Topic;
import org.springframework.stereotype.Service;

@Service("postCreator")
public class PostCreatorServiceImpl implements PostCreatorService {


    @Override
    public Post create(String authorUserName, String topicName, String text) {
        Post post = new Post();
        post.setAuthorUserName(authorUserName);
        post.setTopicName(topicName);
        post.setText(text);
        return post;
    }
}
