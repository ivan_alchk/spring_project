package com.alchuk.demo.service.postservice;

import com.alchuk.demo.entity.Post;

import java.util.List;

public interface PostService {

    void savePost(Post post);
    List<Post> findAllPostsByTopic(String topic);
    List<Post> findAllPostsByAuthor(String author);



}
