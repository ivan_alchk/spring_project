package com.alchuk.demo.service.postservice;

import com.alchuk.demo.dao.postdao.PostDao;
import com.alchuk.demo.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("postService")
public class PostServiceImpl implements PostService {

    @Autowired
    private PostDao postDao;


    @Override
    public void savePost(Post post) {
        postDao.save(post);
    }

    @Override
    public List<Post> findAllPostsByTopic(String topic) {
        return postDao.findAllByTopicName(topic);
    }

    @Override
    public List<Post> findAllPostsByAuthor(String author) {
        return postDao.findAllByAuthorUserName(author);
    }
}
