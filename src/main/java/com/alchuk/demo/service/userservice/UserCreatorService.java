package com.alchuk.demo.service.userservice;

import com.alchuk.demo.entity.User;
import org.springframework.stereotype.Service;

@Service
public interface UserCreatorService {

    User create(String userName, String password);

}
