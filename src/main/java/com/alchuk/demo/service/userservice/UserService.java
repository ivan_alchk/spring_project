package com.alchuk.demo.service.userservice;

import com.alchuk.demo.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface UserService {

    void saveUser(User user);

    Optional<User> findUserByUserNameAndPassword(String userName, String password);

    List<User> getAllUsers();
    User getUserById(long id);

}
