package com.alchuk.demo.service.userservice;

import com.alchuk.demo.dao.userdao.UserDao;
import com.alchuk.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service("userService")
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;


    @Override
    public void saveUser(User user) {
        userDao.save(user);

    }

    @Override
    public Optional<User> findUserByUserNameAndPassword(String userName, String password) {
        return userDao.findUserByUserNameAndPassword(userName, password);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.findAll();
    }

    @Override
    public User getUserById(long id) {
        return userDao.getUserById(id);
    }
}
