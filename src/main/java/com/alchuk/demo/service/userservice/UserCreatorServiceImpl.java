package com.alchuk.demo.service.userservice;

import com.alchuk.demo.entity.User;
import org.springframework.stereotype.Service;



@Service("userCreator")
public class UserCreatorServiceImpl implements UserCreatorService {



    @Override
    public User create(String userName, String password) {
        return prepareAndCheckFields(userName.trim(), password.trim());
    }

    private static User prepareAndCheckFields(String userName, String password) {
        User user = new User();

        user.setUserName(userName);
        user.setPassword(password);


        return user;

    }




}
