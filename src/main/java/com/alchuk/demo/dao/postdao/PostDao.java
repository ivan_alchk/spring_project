package com.alchuk.demo.dao.postdao;

import com.alchuk.demo.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostDao extends JpaRepository<Post, Long> {
    List<Post> findAllByAuthorUserName(String authorUserName);
    List<Post> findAllByTopicName(String topicName);
}
