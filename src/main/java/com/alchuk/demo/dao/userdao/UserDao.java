package com.alchuk.demo.dao.userdao;

import com.alchuk.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

    Optional<User> findUserByUserNameAndPassword(String userName, String password);
    User getUserById(long id);
}
