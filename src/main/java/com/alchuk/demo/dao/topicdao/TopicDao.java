package com.alchuk.demo.dao.topicdao;

import com.alchuk.demo.entity.Topic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicDao extends JpaRepository<Topic, Long> {

}
