package com.alchuk.demo.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users")
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter(AccessLevel.PUBLIC)
    private long id;

    @Column(name = "password")
    @Getter(AccessLevel.PUBLIC)
    @Setter(AccessLevel.PUBLIC)
    private String password;

    @Column(name = "userName")
    @Getter(AccessLevel.PUBLIC)
    @Setter(AccessLevel.PUBLIC)
    private String userName;





}
