package com.alchuk.demo.entity;

import lombok.*;


import javax.persistence.*;

@Data
@Entity
@Table(name = "topics")
@ToString
public class Topic{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter(AccessLevel.PUBLIC)
    private long id;

    @Getter(AccessLevel.PUBLIC)
    @Setter(AccessLevel.PUBLIC)
    @Column(name = "topicName")
    private String topicName;
}
